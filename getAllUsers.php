<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="styles.css">
<meta charset="ISO-8859-1">
<title>Blog Users</title>
</head>

<h1>Blog Users</h1><br/>
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$database_name = "blog_data";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $database_name);

// Check connection
if (!$connection) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully";

$sql = "SELECT * FROM blog_elements";
$result = mysqli_query($connection,$sql);

echo"<table border = '2'>";
echo"<tr><td>ID</td><td>First Name</td><td>Last Name</td></tr>";
while ($row = mysqli_fetch_assoc($result)){
    echo"<tr><td>{$row['ID']}</td><td>{$row['FIRST_NAME']}</td><td>{$row['LAST_NAME']}</td></tr>";
}
echo"</table>";

if(getUserType() === "admin"){
    echo "<a href ='adminLoginResponse.php'>Return Home</a><br />";
}
else{
    echo "<a href ='loginResponse.php'>Return Home</a><br />";}
    
mysqli_close($connection);
?>
<br />

</form>

</html>