<?php

function dbConnect(){
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $database_name = "blog_data";
    
    // Create connection
    global $connection;
    $connection = mysqli_connect($servername, $username, $password, $database_name);
    
    // Check connection
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //echo "Connected successfully";
    
}


function saveUserID($id) {
    session_start();
    $_SESSION['USER_ID'] = $id;
}

function getUserID() {
    session_start();
    return $_SESSION['USER_ID'];
}

function saveUserType($type){
    session_start();
    $_SESSION['USER_TYPE'] = $type;
}

function getUserType(){
    session_start();
    return $_SESSION['USER_TYPE'];
}
